import { render, screen, fireEvent } from "@testing-library/react";
import App from "./App";
import {
  Grid,
  Snackbar,
  FormControl,
  Alert,
  TextField,
  Button,
  Container,
  createTheme,
  ThemeProvider,
  Select,
  MenuItem,
  InputLabel,
  Chip,
  Paper,
  Table,
  TableContainer,
  TableBody,
  TableRow,
  TableCell,
  CircularProgress,
} from "@mui/material";

const key = "87ffe491de0b4e2a9e66f73c817dff8d";
const secret = "Q$YrZ+bQvSRrvqtNRyQ@7j^eK+JWYB&e";

describe("User arrives at API Credentials step (initial)", () => {
  it("blank inputs should trigger errors on submission", async () => {
    const { findByTestId, debug } = render(<App />);
    const submitButton = await findByTestId("data-submitter");
    fireEvent.click(submitButton);
    expect(screen.getByText("Key cannot be blank")).toBeTruthy();
    expect(screen.getByText("Secret cannot be blank")).toBeTruthy();
  });

  it("incorrect length values in either input should trigger errors", async () => {
    const { findByTestId, debug } = render(<App />);
    const submitButton = await findByTestId("data-submitter");
    const keyInput = await findByTestId("api-key-input");
    const secretInput = await findByTestId("api-secret-input");
    fireEvent.change(keyInput, { target: { value: "1111" } });
    fireEvent.change(secretInput, {
      target: { value: "asdkjnbflaksjdfkljnasdflkjnasdlkjfnasdfklnj" },
    });
    fireEvent.click(submitButton);
    expect(screen.getByText("Key length is incorrect")).toBeTruthy();
    expect(screen.getByText("Secret length is incorrect")).toBeTruthy();
  });

  it("should navigate on submission for valid credentials", async () => {
    const { findByTestId, debug } = render(<App />);
    const submitButton = await findByTestId("data-submitter");
    const apiInput = await findByTestId("api-key-input");
    const secretInput = await findByTestId("api-secret-input");
    expect(screen.getByText("API Credentials")).toBeTruthy();
    fireEvent.change(apiInput, { target: { value: key } });
    fireEvent.change(secretInput, { target: { value: secret } });

    fireEvent.click(submitButton);
    expect(screen.getByText("Check Credentials")).toBeTruthy();
    expect(submitButton.textContent).toBe("Submit");
  });
});

describe("User arrives at Check Credentials step", () => {
  it("blank password input should trigger errors on submission", async () => {
    const { findByTestId, debug } = render(<App />);
    const submitButton = await findByTestId("data-submitter");
    const apiInput = await findByTestId("api-key-input");
    const secretInput = await findByTestId("api-secret-input");
    fireEvent.change(apiInput, { target: { value: key } });
    fireEvent.change(secretInput, { target: { value: secret } });
    expect(screen.getByText("API Credentials")).toBeTruthy();
    fireEvent.click(submitButton);
    expect(screen.getByText("Check Credentials")).toBeTruthy();
    // End pre-nav
    const select = await findByTestId("api-select");
    fireEvent.click(submitButton);
    expect(screen.getByText("Password cannot be blank")).toBeTruthy();
    fireEvent.change(select, { target: { value: "username" } });
    fireEvent.click(submitButton);
    expect(screen.getByText("Username cannot be blank")).toBeTruthy();
  });

  it("blank username input should trigger errors on submission", async () => {
    const { findByTestId, debug } = render(<App />);
    const submitButton = await findByTestId("data-submitter");
    const apiInput = await findByTestId("api-key-input");
    const secretInput = await findByTestId("api-secret-input");
    fireEvent.change(apiInput, { target: { value: key } });
    fireEvent.change(secretInput, { target: { value: secret } });
    expect(screen.getByText("API Credentials")).toBeTruthy();
    fireEvent.click(submitButton);
    expect(screen.getByText("Check Credentials")).toBeTruthy();
    // End pre-nav
    const select = await findByTestId("api-select");
    fireEvent.change(select, { target: { value: "username" } });
    fireEvent.click(submitButton);
    expect(screen.getByText("Username cannot be blank")).toBeTruthy();
  });

  it("blank credentials inputs should trigger errors on submission", async () => {
    const { findByTestId, debug } = render(<App />);
    const submitButton = await findByTestId("data-submitter");
    const apiInput = await findByTestId("api-key-input");
    const secretInput = await findByTestId("api-secret-input");
    fireEvent.change(apiInput, { target: { value: key } });
    fireEvent.change(secretInput, { target: { value: secret } });
    expect(screen.getByText("API Credentials")).toBeTruthy();
    fireEvent.click(submitButton);
    expect(screen.getByText("Check Credentials")).toBeTruthy();
    // End pre-nav
    const select = await findByTestId("api-select");
    fireEvent.change(select, { target: { value: "credentials" } });
    fireEvent.click(submitButton);
    expect(screen.getByText("Username cannot be blank")).toBeTruthy();
    expect(screen.getByText("Password cannot be blank")).toBeTruthy();
  });

  it("should navigate on submission for valid input", async () => {
    const { findByTestId, debug } = render(<App />);
    const submitButton = await findByTestId("data-submitter");
    const apiInput = await findByTestId("api-key-input");
    const secretInput = await findByTestId("api-secret-input");
    fireEvent.change(apiInput, { target: { value: key } });
    fireEvent.change(secretInput, { target: { value: secret } });
    expect(screen.getByText("API Credentials")).toBeTruthy();
    fireEvent.click(submitButton);
    expect(screen.getByText("Check Credentials")).toBeTruthy();
    // End pre-nav
    const passwordInput = await findByTestId("api-input1");
    fireEvent.change(passwordInput, { target: { value: "password" } });
    fireEvent.click(submitButton);
    const compromisedResponse = await findByTestId("compromised-response");
    expect(compromisedResponse.textContent).toBe(
      "Unfortunately, your password has been compromised."
    );
    expect(submitButton.textContent).toBe("Check Another");
  });
});
