import axios from "axios";
import React from "react";
import {
  Grid,
  Snackbar,
  FormControl,
  Alert,
  TextField,
  Button,
  Container,
  createTheme,
  ThemeProvider,
  Select,
  MenuItem,
  InputLabel,
  Chip,
  Paper,
  Table,
  TableContainer,
  TableBody,
  TableRow,
  TableCell,
  CircularProgress,
} from "@mui/material";
import BuildIcon from "@mui/icons-material/Build";
import NewReleasesIcon from "@mui/icons-material/NewReleases";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import "./App.scss";

function App() {
  const [APICredentials, setAPICredentials] = React.useState({
    key: "",
    secret: "",
  });
  const [steps] = React.useState(["credentials", "api", "results"]);
  const [step, setStep] = React.useState(steps[0]);
  const [errors, setErrors] = React.useState(false);
  const [secretError, setSecretError] = React.useState(false);
  const [keyError, setKeyError] = React.useState(false);
  const [input1Value, setInput1Value] = React.useState("");
  const [input2Value, setInput2Value] = React.useState("");
  const [input1Error, setInput1Error] = React.useState(false);
  const [input2Error, setInput2Error] = React.useState(false);
  const [compromised, setCompromised] = React.useState(false);
  const [exposureIDs, setExposureIDs] = React.useState([]);
  const [labels] = React.useState(["Next", "Submit", "Check Another"]);
  const [currentStepLabel, setCurrentStepLabel] = React.useState(labels[0]);
  const [exposureDetail, setExposureDetail] = React.useState([]);
  const [currentExposureID, setCurrentExposureID] = React.useState("");
  const [APIOptions] = React.useState([
    {
      title: "Password",
      value: "password",
    },
    {
      title: "Username",
      value: "username",
    },
    {
      title: "Username/Password Combination",
      value: "credentials",
    },
  ]);
  const [currentAPIOption, setCurrentAPIOption] = React.useState(
    APIOptions[0].value
  );

  const theme = createTheme({
    palette: {
      primary: {
        main: "#232f38",
      },
      secondary: {
        main: "#ed1e24",
      },
      white: {
        main: "#ffffff",
      },
    },
  });

  const validateCredentials = () => {
    let newErrorsArray = errors ? [errors] : [];

    if (APICredentials.key === "") {
      newErrorsArray.push("Key cannot be blank");
      setKeyError(true);
    } else if (APICredentials.key.length !== 32) {
      newErrorsArray.push("Key length is incorrect");
      setKeyError(true);
    }

    if (APICredentials.secret === "") {
      newErrorsArray.push("Secret cannot be blank");
      setSecretError(true);
    } else if (APICredentials.secret.length !== 32) {
      newErrorsArray.push("Secret length is incorrect");
      setSecretError(true);
    }
    if (newErrorsArray.length >= 1) {
      setErrors(newErrorsArray);
    } else {
      nextStep();
    }
  };
  const resetErrors = () => {
    setKeyError(false);
    setSecretError(false);
    setErrors(false);
  };

  const getLabel = (formNumber) => {
    let currentIndex = APIOptions.map(function (e) {
      return e.value;
    }).indexOf(currentAPIOption);
    let title;
    if (currentAPIOption === "credentials") {
      if (formNumber === 1) {
        title = "Username";
      } else {
        title = "Password";
      }
    } else {
      title = APIOptions[currentIndex].title;
    }
    return title;
  };

  const validateAPIData = () => {
    let newErrorsArray = [];
    if (input1Value === "") {
      setInput1Error(true);
      newErrorsArray.push(getLabel(1) + " cannot be blank");
    }
    if (currentAPIOption === "credentials" && input2Value === "") {
      setInput2Error(true);
      newErrorsArray.push(getLabel(2) + " cannot be blank");
    }
    if (newErrorsArray.length >= 1) {
      setErrors(newErrorsArray);
    } else {
      setCurrentStepLabel("loading");
      callAPI();
    }
  };

  const callAPI = async (id) => {
    let data = APICredentials;
    let baseRoute = "http://localhost:3030/";
    let route;
    if (currentAPIOption === "credentials") {
      data["username"] = input1Value;
      data["password"] = input2Value;
    } else if (step === "results") {
      data["exposureID"] = id;
    } else {
      data[currentAPIOption] = input1Value;
    }

    if (step === "results") {
      route = baseRoute + "exposure-id";
    } else {
      route = baseRoute + currentAPIOption;
    }
    axios
      .post(route, data)
      .then((res) => {
        if (step !== "results") {
          setCompromised(res.data.compromised);
          if (currentAPIOption === "username") {
            setExposureIDs(res.data.exposureIDs);
          }
          nextStep();
        } else {
          let detailArray = Object.keys(res.data).map((key) => ({
            key: key,
            value: res.data[key],
          }));
          setExposureDetail(detailArray);
        }
      })
      .catch(function (error) {
        setErrors([error.toJSON()]);
        return Promise.reject(error);
      });
  };

  const goBack = () => {
    let currentStepIndex = steps.indexOf(step);
    setStep(steps[currentStepIndex - 1]);
    let currentLabelIndex = labels.indexOf(currentStepLabel);
    console.log(steps[currentStepIndex - 1]);
    console.log(labels[currentLabelIndex - 1]);
    setCurrentStepLabel(labels[currentLabelIndex - 1]);
  };

  const nextStep = () => {
    let currentStepIndex = steps.indexOf(step);
    setStep(steps[currentStepIndex + 1]);
    let currentLabelIndex = labels.indexOf(currentStepLabel);
    setCurrentStepLabel(labels[currentLabelIndex + 1]);
  };

  const getCurrentStepLabel = () => {
    if (currentStepLabel === "loading") {
      return <CircularProgress color="white" />;
    } else {
      return currentStepLabel;
    }
  };

  const submitHandler = () => {
    switch (step) {
      case "credentials":
        validateCredentials();
        break;
      case "api":
        validateAPIData();
        break;
      case "results":
        setCurrentExposureID("");
        setExposureDetail([]);
        setExposureIDs([]);
        setInput1Value("");
        setInput2Value("");
        goBack();
        break;
      default:
        break;
    }
  };

  const getExposureDetails = (exposureID) => {
    setCurrentExposureID("");
    setCurrentExposureID(exposureID);
    callAPI(exposureID);
  };

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <header className="App-header">
          <Snackbar
            open={Array.isArray(errors)}
            autoHideDuration={5000}
            onClose={() => setErrors(false)}
          >
            <Alert
              onClose={() => setErrors(false)}
              severity="error"
              sx={{ width: "50%" }}
            >
              <span id="snackbar">
                Please check the form and submit again, the following errors
                were found:
              </span>
              <ul>
                {errors &&
                  errors.length >= 1 &&
                  errors.map((l, i) => (
                    <li key={i} data-testid={l.testId}>
                      {l}
                    </li>
                  ))}
              </ul>
            </Alert>
          </Snackbar>
        </header>
        <div className="mainOC">
          <Paper className="panelOC">
            <h2>Vulnerability Checker</h2>
            <Grid
              container
              rowSpacing={5}
              alignItems="center"
              justifyContent="center"
            >
              {step === "credentials" && (
                <Container maxWidth="md">
                  <h3 data-testid="step-credentials-header">API Credentials</h3>
                  <Grid container rowSpacing={3}>
                    <Grid item xs={12}>
                      <FormControl fullWidth>
                        <TextField
                          onKeyPress={(e) =>
                            e.key === "Enter" && submitHandler()
                          }
                          id="api-key"
                          label="Key"
                          inputProps={{ "data-testid": "api-key-input" }}
                          variant="outlined"
                          fullWidth
                          value={APICredentials.key}
                          error={keyError}
                          onFocus={() => resetErrors()}
                          onChange={(e) =>
                            setAPICredentials({
                              ...APICredentials,
                              key: e.target.value,
                            })
                          }
                        />
                      </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                      <FormControl fullWidth>
                        <TextField
                          onKeyPress={(e) =>
                            e.key === "Enter" && submitHandler()
                          }
                          id="api-secret"
                          label="Secret"
                          inputProps={{ "data-testid": "api-secret-input" }}
                          variant="outlined"
                          value={APICredentials.secret}
                          error={secretError}
                          onFocus={() => resetErrors()}
                          onChange={(e) =>
                            setAPICredentials({
                              ...APICredentials,
                              secret: e.target.value,
                            })
                          }
                          fullWidth
                        />{" "}
                      </FormControl>
                    </Grid>
                  </Grid>
                </Container>
              )}

              {step === "api" && (
                <Container maxWidth="md">
                  <Grid container columnSpacing={0}>
                    <Grid item xs={12} lg={6}>
                      <Chip
                        onClick={() => goBack()}
                        label={
                          <div>
                            <span>{"Key: " + APICredentials.key}&nbsp;</span>
                            <BuildIcon sx={{ fontSize: 12 }} />
                          </div>
                        }
                      />
                    </Grid>
                    <Grid item xs={12} lg={6}>
                      <Chip
                        onClick={() => goBack()}
                        label={
                          <div>
                            <span>
                              {"Secret: " + APICredentials.secret}&nbsp;
                            </span>
                            <BuildIcon sx={{ fontSize: 12 }} />
                          </div>
                        }
                      />
                    </Grid>
                  </Grid>
                  <h3 data-testid="step-api-header">Check Credentials</h3>
                  <Grid container rowSpacing={3}>
                    <Grid item xs={12}>
                      <FormControl fullWidth>
                        <InputLabel id="api">API Type</InputLabel>
                        <Select
                          labelId="api"
                          onKeyPress={(e) =>
                            e.key === "Enter" && submitHandler()
                          }
                          id="api-option-select"
                          inputProps={{ "data-testid": "api-select" }}
                          value={currentAPIOption}
                          label="API Type"
                          color="primary"
                          onChange={(e) => setCurrentAPIOption(e.target.value)}
                        >
                          {APIOptions.map((l, i) => (
                            <MenuItem key={i} value={l.value}>
                              {l.title}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                      <FormControl fullWidth>
                        <TextField
                          onKeyPress={(e) =>
                            e.key === "Enter" && submitHandler()
                          }
                          id={currentAPIOption + "-1"}
                          label={getLabel(1)}
                          variant="outlined"
                          fullWidth
                          inputProps={{ "data-testid": "api-input1" }}
                          value={input1Value}
                          error={input1Error}
                          onFocus={() => resetErrors()}
                          onChange={(e) => setInput1Value(e.target.value)}
                        />
                      </FormControl>
                    </Grid>
                    {currentAPIOption === "credentials" && (
                      <Grid item xs={12}>
                        <FormControl fullWidth>
                          <TextField
                            onKeyPress={(e) =>
                              e.key === "Enter" && submitHandler()
                            }
                            id={currentAPIOption + "-2"}
                            label={getLabel(2)}
                            inputProps={{ "data-testid": "api-input2" }}
                            variant="outlined"
                            error={input2Error}
                            onFocus={() => resetErrors()}
                            fullWidth
                            value={input2Value}
                            onChange={(e) => setInput2Value(e.target.value)}
                          />
                        </FormControl>
                      </Grid>
                    )}
                  </Grid>
                </Container>
              )}

              {step === "results" && (
                <Container maxWidth="md">
                  <Grid
                    container
                    alignItems="center"
                    justifyContent="center"
                    className="resultsOC"
                  >
                    <Grid
                      container
                      alignItems="center"
                      justifyContent="center"
                      className="resultsOC"
                    >
                      <Grid item xs={10}>
                        {compromised ? (
                          <NewReleasesIcon fontSize="large" color="secondary" />
                        ) : (
                          <CheckCircleIcon fontSize="large" color="success" />
                        )}
                      </Grid>
                      <Grid item xs={10}>
                        <span data-testid="compromised-response">
                          {compromised
                            ? "Unfortunately, your " +
                              getLabel().toLowerCase() +
                              " has been compromised."
                            : "Congrats, looks like your " +
                              getLabel().toLowerCase() +
                              " has not been compromised."}
                        </span>
                      </Grid>
                    </Grid>
                    {exposureIDs.length >= 1 && (
                      <Grid item xs={8}>
                        <FormControl fullWidth>
                          <InputLabel id="api">Exposure IDs</InputLabel>
                          <Select
                            labelId="api"
                            id="api-option-select"
                            inputProps={{ "data-testid": "exposure-ids-select" }}
                            value={currentExposureID}
                            label="Exposure IDs"
                            color="primary"
                            onChange={(e) => getExposureDetails(e.target.value)}
                          >
                            {exposureIDs.map((l, i) => (
                              <MenuItem key={l} value={l}>
                                {l}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Grid>
                    )}

                    {currentExposureID !== "" && exposureDetail.length === 0 && (
                      <Grid item xs={8}>
                        <CircularProgress color="primary" />
                      </Grid>
                    )}
                    {exposureDetail.length >= 1 && (
                      <Grid item xs={8}>
                        <TableContainer component={Paper} data-testid="detail-table">
                          <Table size="small" aria-label="a dense table">
                            <TableBody>
                              {exposureDetail.map((l, i) => (
                                <TableRow
                                  key={l.key}
                                  sx={{
                                    "&:last-child td, &:last-child th": {
                                      border: 0,
                                    },
                                  }}
                                >
                                  <TableCell component="th" scope="row">
                                    {l.key}
                                  </TableCell>
                                  <TableCell align="right">{l.value}</TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      </Grid>
                    )}
                  </Grid>
                </Container>
              )}
              <Grid item xs={12}>
                <Button
                  className="submit-button"
                  color="secondary"
                  data-testid="data-submitter"
                  fullWidth
                  onClick={() => submitHandler()}
                  variant="contained"
                >
                  {getCurrentStepLabel()}
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </div>
      </ThemeProvider>
    </div>
  );
}

export default App;
