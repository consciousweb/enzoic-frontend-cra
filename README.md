## Installation

1. Install packages

	$ npm i

2. Ensure feathers server/api application is running on http://localhost:3030/

3. Run application

	$ npm start

Enjoy!


## Testing

Run test suite with:

	$ npm test